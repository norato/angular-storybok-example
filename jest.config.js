module.exports = {
  preset: 'jest-preset-angular',
  verbose: false,
  setupTestFrameworkScriptFile: '<rootDir>/src/setup-jest.ts'
};
