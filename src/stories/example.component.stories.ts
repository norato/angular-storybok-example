import { storiesOf } from "@storybook/angular";
import { metadata } from "./utils";
import { withTheme } from "./addons/wrap";

storiesOf("Custom | Angular / Example Component", module)
  .addDecorator(metadata)
  .addDecorator(withTheme)
  .add(
    "with props in template",
    () => ({
      template: `
    <div class="box">
      <app-example [text]="variable"></app-example>
    </div>
    `,
      props: {
        variable: "amazing variable"
      }
    }),
    { jest: "example.component" }
  );
