//  Solution from: https://github.com/storybooks/storybook/issues/2962#issuecomment-411339557
import { boolean, withKnobs } from "@storybook/addon-knobs";

export const wrap = templateFn => storyFn => {
  const story = storyFn();
  return {
    ...story,
    template: templateFn(story.template)
  };
};

export const gridWrap = wrap(
  content => `<app-grid-overlay *ngIf="showgrid"></app-grid-overlay>${content}`
);

export const withTheme = (story, context) => {
  const storyWithKnobs = withKnobs(story, context);

  return {
    ...storyWithKnobs,
    props: {
      ...storyWithKnobs.props,
      showgrid: boolean("Show Grid Overlay", false)
    },
    template: `<app-grid-overlay *ngIf="showgrid"></app-grid-overlay>${
      storyWithKnobs.template
    }`
  };
};
